import { createRouter, createWebHistory } from "vue-router";
import ListOfPosts from "../components/ListOfPosts.vue";
import SinglePost from "../components/SinglePost.vue";

const routes = [
  {
    path: "/post/:id",
    component: SinglePost,
    name: "single-post",
  },
  {
    path: "/",
    component: ListOfPosts,
    name: "home",
  },
  {
    path: "/:pathMatch(.*)",
    redirect: () => {
      return { path: "/" };
    },
  },
];

const history = createWebHistory("/blog-public/");

export const router = createRouter({
  history,
  routes,
});
