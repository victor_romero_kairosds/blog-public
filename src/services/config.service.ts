import axios from "axios";

export interface Config {
  API_URL: string;
}

export class ConfigService {
  static config: Config;

  static async loadConfig() {
    const { data } = await axios.get("/blog-public/config/config.json");
    this.config = data;
  }

  static get env(): Config {
    return this.config;
  }
}
