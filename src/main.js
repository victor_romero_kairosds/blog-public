import { createApp } from "vue";
import App from "./App.vue";

import { router } from "./router";
import { ConfigService } from "./services/config.service";
import "./style.css";

async function AppInitialization() {
  await ConfigService.loadConfig();
  createApp(App).use(router).mount("#app");
}

AppInitialization();
