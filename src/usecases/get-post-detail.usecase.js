import { PostRepository } from "../repository/post.repository";

export class GetPostDetailUseCase {
  async execute(postId) {
    const repository = new PostRepository();
    return repository.getPostById(postId);
  }
}
