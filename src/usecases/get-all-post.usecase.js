import { PostRepository } from "../repository/post.repository";

export class GetAllPostsUseCase {
  async execute() {
    const repository = new PostRepository();
    return repository.getAllPosts();
  }
}
