import axios from "axios";
import { ConfigService as Config } from "../services/config.service";

export class PostRepository {
  async getAllPosts() {
    const { data } = await axios.get(`${Config.env.API_URL}/post`);
    return data;
  }

  async getPostById(id) {
    const { data } = await axios.get(`${Config.env.API_URL}/post/${id}`);
    return data;
  }
}
