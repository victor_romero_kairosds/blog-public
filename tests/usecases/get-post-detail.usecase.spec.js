import { expect, it, vi } from "vitest";

const POST_DETAIL = {
  id: "e5f297ba-b390-4236-8032-4e7f4bd3b333",
  title: "Title 2",
  content: "Content 2",
  author: "author@test.com",
  comments: [
    {
      id: "2cfb8da7-0c3c-40f3-8eea-950b898484ea",
      content: "Hello World",
      author: "admin@test.com",
    },
    {
      id: "ad455711-d231-4336-8633-9baf297cb65e",
      content: "Prueba 3",
      author: "admin@test.com",
    },
    {
      id: "52059f74-00b0-4939-a47e-6e1519ee18f4",
      content: "Hola mundo",
      author: "admin@test.com",
    },
  ],
};

vi.mock("../../src/repository/post.repository", () => {
  return {
    PostRepository: vi.fn().mockImplementation(() => {
      return {
        getPostById: vi.fn().mockImplementation(() => {
          return POST_DETAIL;
        }),
      };
    }),
  };
});

import { GetPostDetailUseCase } from "../../src/usecases/get-post-detail.usecase";

describe("Get post detail use case...", () => {
  it("should execute correctly, returning an array of posts", async () => {
    const useCase = new GetPostDetailUseCase();
    const response = await useCase.execute();
    expect(response).toEqual(POST_DETAIL);
    expect(response.comments).toHaveLength(3);
  });
});
