import { expect, it, vi } from "vitest";

const ALL_POSTS = [
  {
    id: "8d900ae6-b918-44ac-88b7-7616b008bc83",
    title: "Title 1",
    content: "Content 1",
    author: "admin@test.com",
  },
  {
    id: "e5f297ba-b390-4236-8032-4e7f4bd3b333",
    title: "Title 2",
    content: "Content 2",
    author: "author@test.com",
  },
];

vi.mock("../../src/repository/post.repository", () => {
  return {
    PostRepository: vi.fn().mockImplementation(() => {
      return {
        getAllPosts: vi.fn().mockImplementation(() => {
          return ALL_POSTS;
        }),
      };
    }),
  };
});

import { GetAllPostsUseCase } from "../../src/usecases/get-all-post.usecase";

describe("Get all posts use case...", () => {
  it("should execute correctly, returning an array of posts", async () => {
    const useCase = new GetAllPostsUseCase();
    const response = await useCase.execute();
    expect(response).toHaveLength(2);
    expect(response).toEqual(ALL_POSTS);
  });
});
