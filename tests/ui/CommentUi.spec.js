import { render } from "@testing-library/vue";
import { describe, it } from "vitest";
import { router } from "../../src/router";

import CommentUi from "../../src/ui/CommentUi.vue";

describe("CommentUi component", () => {
  it("renders post data when passed from props", () => {
    const comment = {
      id: "1",
      content: "Lorem ipsum dolor",
      author: "Victor Romero",
    };

    const component = render(CommentUi, {
      props: { comment },
      global: { plugins: [router] },
    });
    component.getByText("Victor Romero");
    component.getByText("Lorem ipsum dolor");
  });
});
