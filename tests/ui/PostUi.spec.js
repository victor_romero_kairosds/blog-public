import { render } from "@testing-library/vue";
import { describe, it } from "vitest";
import { router } from "../../src/router";

import PostUi from "../../src/ui/PostUi.vue";

describe("PostUi component", () => {
  it("renders post data when passed from props", () => {
    const post = {
      id: "1",
      title: "Random title",
      author: "Victor Romero",
      content: "Lorem ipsum dolor",
    };

    const component = render(PostUi, {
      props: { post },
      global: { plugins: [router] },
    });
    component.getByRole("link", { name: /Random title/ });
    component.getByText("Victor Romero");
    component.getByText("Lorem ipsum dolor");
  });
});
