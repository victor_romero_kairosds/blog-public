import vue from "@vitejs/plugin-vue";
import { defineConfig } from "vitest/config";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: "/blog-public/",
  test: {
    globals: true,
    environment: "jsdom",
  },
});
